package com.thecode.datezone.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.thecode.datezone.R;
import com.thecode.datezone.activities.Hold;


public class ZoneFragment extends Fragment {
    View view;
    Button btn_adult_services;
    Button btn_hookup_services;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_zone, container, false);
        btn_adult_services=(Button) view.findViewById(R.id.btn_dating);
        btn_hookup_services=view.findViewById(R.id.btn_AdultHookup);
        //Adult services listener
        btn_adult_services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                AdultServices();
                Intent intent = new Intent(getContext(), Hold.class);
                intent.putExtra("ButtonID", "Registration");
                getContext().startActivity(intent);

            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    private void AdultServices() {
        //Show !8+ dialog
        Dialog dialog = new Dialog(view.getContext(), R.style.DialogStyle);
        dialog.setContentView(R.layout.custom_dialog_box);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_window);
        ImageView btnClose = dialog.findViewById(R.id.btn_close);
        Button btn_no=dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
//        des.setGravity(0,0,0,0);
        Button yes=dialog.findViewById(R.id.btn_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ContentRegister fragment = new ContentRegister();
//                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.Container, fragment);
//                fragmentTransaction.commit();

                Intent intent = new Intent(getContext(), Hold.class);
                intent.putExtra("ButtonID", "Registration");
                getContext().startActivity(intent);
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }
}