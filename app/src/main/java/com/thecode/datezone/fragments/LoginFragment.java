package com.thecode.datezone.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.thecode.datezone.R;
import com.thecode.datezone.activities.Authentication;
import com.thecode.datezone.activities.Hold;
import com.thecode.datezone.activities.WelcomActivity;

public class LoginFragment extends Fragment {
    View view;
    TextView signUp;
    Button btn_login;
    EditText et_email_login;
    EditText et_password_login;
    FirebaseAuth firebaseAuth;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);
        signUp = view.findViewById(R.id.txt_SignUpFromLogin);
        btn_login=view.findViewById(R.id.btn_SignIn);
        et_email_login=view.findViewById(R.id.et_EmailLogin);
        et_password_login=view.findViewById(R.id.et_PasswordLogin);
        firebaseAuth = FirebaseAuth.getInstance();
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
login();
            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.RegisterContainer,new RegistrationFragment());
                fragmentTransaction.commit();
            }
        });
        return view;
    }
    public void login()
    {
        String email=et_email_login.getText().toString();
        String password=et_password_login.getText().toString();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(view.getContext(), "Field Must Filled", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(view.getContext(), "Field Must Filled", Toast.LENGTH_SHORT).show();
        }else if (!email.matches(emailPattern)) {
           et_email_login.setError("Enter Valid Email");
            Toast.makeText(view.getContext(), "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
        }else if (password.length() < 6) {
            Toast.makeText(view.getContext(), "Enter 6 Character Password", Toast.LENGTH_SHORT).show();
        } else
        {
            firebaseAuth.signInWithEmailAndPassword(et_email_login.getText().toString(),et_password_login.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {


                            if(task.isSuccessful()){
                                Toast.makeText(getContext(),"Login successful",Toast.LENGTH_LONG).show();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.RegisterContainer,new CreateProfileFragment());
                                fragmentTransaction.commit();
                                 }
                            else{
                                Log.e("ERROR",task.getException().toString());
                                Toast.makeText(getContext(), task.getException().getMessage(),Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

}