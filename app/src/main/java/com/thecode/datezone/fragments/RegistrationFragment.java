package com.thecode.datezone.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.util.Half;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.thecode.datezone.R;
import com.thecode.datezone.activities.Authentication;
import com.thecode.datezone.activities.Hold;
import com.thecode.datezone.activities.WelcomActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistrationFragment extends Fragment {

    View view;
    EditText et_username;
    EditText et_email;
    EditText et_password;
    EditText et_confirm_password;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    FirebaseAuth firebaseAuth;

    FirebaseDatabase firebaseDatabase;
    private ProgressDialog mLoadingBar;
    Button btn_register;
    TextView signIn;
    EditText birthdate;
    Calendar myCalender;
    int age;
    int year;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_registration, container, false);
        signIn = view.findViewById(R.id.txt_SignInFromRegister);
        btn_register = view.findViewById(R.id.btn_SignUpRegister);
        birthdate = view.findViewById(R.id.et_birthRegister);
        et_username = view.findViewById(R.id.et_UsernameRegister);
        et_email = view.findViewById(R.id.et_Email_Register);
        et_password = view.findViewById(R.id.et_PasswordRegister);
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        et_confirm_password = view.findViewById(R.id.et_ConfirmPasswordRegister);
        myCalender = Calendar.getInstance();
        year = myCalender.get(Calendar.YEAR);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalender.set(Calendar.YEAR, year);
                myCalender.set(Calendar.MONTH, month);
                myCalender.set(Calendar.DAY_OF_MONTH, day);

                updateLabel();
            }
        };
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), date, myCalender.get(Calendar.YEAR), myCalender.get(Calendar.MONTH), myCalender.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Register();
            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrationFragment registrationFragment = new RegistrationFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.RegisterContainer, new LoginFragment());

                fragmentTransaction.remove(registrationFragment).commit();
            }
        });
        return view;
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.US);
        birthdate.setText(dateFormat.format(myCalender.getTime()));
        age = year - myCalender.get(Calendar.YEAR);
        Log.d("TAG", "updateLabel: " + age);
    }

    private void AdultServices() {
        //Show !8+ dialog
        Dialog dialog = new Dialog(view.getContext(), R.style.DialogStyle);
        dialog.setContentView(R.layout.custom_dialog_box);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_window);
        ImageView btnClose = dialog.findViewById(R.id.btn_close);
        Button btn_no = dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
//        des.setGravity(0,0,0,0);
        Button yes = dialog.findViewById(R.id.btn_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ContentRegister fragment = new ContentRegister();
//                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.Container, fragment);
//                fragmentTransaction.commit();
//
//                Intent intent = new Intent(getContext(), Hold.class);
//                intent.putExtra("ButtonID", "Registration");
//                getContext().startActivity(intent);
                if (age >= 18) {
                    RegistrationFragment registrationFragment = new RegistrationFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.RegisterContainer, new LoginFragment());

                    fragmentTransaction.remove(registrationFragment).commit();
                    dialog.dismiss();
                } else {
                    Toast.makeText(getContext(), "Must be 18+", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void Register() {
        String username = et_username.getText().toString();
        String email = et_email.getText().toString();
        String birth = birthdate.getText().toString();
        String password = et_password.getText().toString();
        String confirmPassword = et_confirm_password.getText().toString();
        if (TextUtils.isEmpty(username)) {
            Toast.makeText(view.getContext(), "Field Must Filled", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(birth)) {
            Toast.makeText(view.getContext(), "Field Must Filled", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(view.getContext(), "Field Must Filled", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(view.getContext(), "Field Must Filled", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(confirmPassword)) {
            Toast.makeText(view.getContext(), "Field Must Filled", Toast.LENGTH_SHORT).show();
        } else if (!email.matches(emailPattern)) {
            et_email.setError("Enter Valid Email");
            Toast.makeText(view.getContext(), "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
        } else if (!password.equals(confirmPassword)) {
            et_password.setError("Password Not Match");
            Toast.makeText(view.getContext(), "Password Not Match", Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(view.getContext(), "Enter 6 Character Password", Toast.LENGTH_SHORT).show();
        } else {
//            mLoadingBar.setMessage("Please wait...");
//            mLoadingBar.setCanceledOnTouchOutside(false);
//            mLoadingBar.show();

            firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        {
                            //mLoadingBar.dismiss();
                            new SweetAlertDialog(view.getContext(), 2).setTitleText("Registered Successfully").show();
                            AdultServices();
                            new SweetAlertDialog(view.getContext(),2).dismiss();
                            /*firebaseConnection.InsertDataToDb(username, email);*/

                        }
                        et_username.setText("");
                        et_email.setText("");
                        et_password.setText("");
                        et_confirm_password.setText("");
                    } else {
                        Toast.makeText(view.getContext(), "Registration Failed", Toast.LENGTH_SHORT).show();
                        mLoadingBar.dismiss();
                    }
                }
            });
        }

    }
}