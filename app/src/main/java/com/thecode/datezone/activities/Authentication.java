package com.thecode.datezone.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentTransaction;

import com.thecode.datezone.R;
import com.thecode.datezone.fragments.ClassifiedAdsFragment;
import com.thecode.datezone.fragments.LoginFragment;
import com.thecode.datezone.fragments.ZoneFragment;


public class Authentication extends AppCompatActivity {
    String ButtonID;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_authentication);
        this.ButtonID = getIntent().getStringExtra("ButtonID");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.ButtonID.equals("SignUp")) {
            fragmentTransaction.add((int) R.id.Container, (Fragment) new ZoneFragment());
            fragmentTransaction.commit();
        }
        else if (ButtonID.equals("SignIn"))
        {
            fragmentTransaction.add((int) R.id.Container, (Fragment) new LoginFragment());
            fragmentTransaction.commit();
        }

    }
}
