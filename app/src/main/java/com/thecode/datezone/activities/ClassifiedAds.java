package com.thecode.datezone.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.thecode.datezone.R;

public class ClassifiedAds extends AppCompatActivity {
Button btn_signUp;
Button btn_SignIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified_ads);
    btn_signUp=findViewById(R.id.btn_SignUpClassified);
    btn_SignIn=findViewById(R.id.btn_SignInClassified);
    btn_signUp.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ClassifiedAds.this, ClassifiedHolder.class);
            intent.putExtra("ButtonID", "Registrations");
            startActivity(intent);

        }
    });
    btn_SignIn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ClassifiedAds.this, ClassifiedHolder.class);
            intent.putExtra("ButtonID", "Logins");
            startActivity(intent);
        }
    });
    }
}