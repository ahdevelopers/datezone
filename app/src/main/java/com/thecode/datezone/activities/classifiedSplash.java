package com.thecode.datezone.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.thecode.datezone.R;

public class classifiedSplash extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified_splash);

        new Handler().postDelayed(() -> {
            Intent intent = new Intent(this,ClassifiedAds.class);
            startActivity(intent);
            finish();

        }, 2000);
    }
}