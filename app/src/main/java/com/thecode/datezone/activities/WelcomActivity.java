package com.thecode.datezone.activities;


import android.content.Intent;
import android.os.Bundle;


import android.view.View;
import android.widget.Button;


import androidx.appcompat.app.AppCompatActivity;

import com.thecode.datezone.R;


public class WelcomActivity extends AppCompatActivity {
    Button btn_dateZone;
    Button btn_classifiedAds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcom);
        btn_dateZone=findViewById(R.id.btn_datezone);
        btn_classifiedAds=findViewById(R.id.btn_classified_ads);
        this.btn_dateZone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(WelcomActivity.this, Authentication.class);
                intent.putExtra("ButtonID", "SignUp");
                WelcomActivity.this.startActivity(intent);
            }
        });
        this.btn_classifiedAds.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(WelcomActivity.this,classifiedSplash.class));
            }
        });

    }
}