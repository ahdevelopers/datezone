package com.thecode.datezone.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.thecode.datezone.R;
import com.thecode.datezone.fragments.CreateProfileFragment;
import com.thecode.datezone.fragments.LoginFragment;
import com.thecode.datezone.fragments.RegistrationFragment;

public class Hold extends AppCompatActivity {
String ButtonID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hold);
        this.ButtonID = getIntent().getStringExtra("ButtonID");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.ButtonID.equals("Registration")) {
            fragmentTransaction.add((int) R.id.RegisterContainer, (Fragment) new RegistrationFragment());
            fragmentTransaction.commit();
        }
        else  if (this.ButtonID.equals("Login")) {
            fragmentTransaction.add((int) R.id.RegisterContainer, (Fragment) new LoginFragment());
            fragmentTransaction.commit();
        }
        else  if (this.ButtonID.equals("Create")) {
            fragmentTransaction.add((int) R.id.RegisterContainer, (Fragment) new CreateProfileFragment());
            fragmentTransaction.commit();
        }


    }
}