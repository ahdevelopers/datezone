package com.thecode.datezone.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.thecode.datezone.R;
import com.thecode.datezone.fragments.ClassifiedLogin;
import com.thecode.datezone.fragments.ClassifiedRegistration;

public class ClassifiedHolder extends AppCompatActivity {
String ButtonID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified_holder);
        this.ButtonID = getIntent().getStringExtra("ButtonID");
        ClassifiedRegistration classifiedRegistration=new ClassifiedRegistration();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.ButtonID.equals("Registrations")) {
            fragmentTransaction.add((int) R.id.Classified_Container, (Fragment)classifiedRegistration);
            fragmentTransaction.commit();
        } else if (ButtonID.equals("Logins")) {
            fragmentTransaction.add((int) R.id.Classified_Container, (Fragment) new ClassifiedLogin());
            fragmentTransaction.commit();
        }
    }

    }
